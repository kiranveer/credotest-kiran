﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TaskController : Controller
    {
        // GET: Task
        public ActionResult Index()
        {
            //P.S Should get the Url from Web.Config
            string URL = "http://localhost:59824/api/tasks";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync(URL).Result;//.GetAsync(urlParameters).Result; 
            var lstTasks = new List<Models.Task>();
            if (response.IsSuccessStatusCode)
            {
                lstTasks = (List<Models.Task>)response.Content.ReadAsAsync<IEnumerable<Models.Task>>().Result;

            }

            return View(lstTasks);
        }

        // GET: Task/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Task/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Task/Create
        [HttpPost]
        public ActionResult Create(Task task)// FormCollection collection)
        {
            try
            {
                //P.S Should get the Url from Web.Config
                string URL = "http://localhost:59824/api/tasks";
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
                //var objTask = JsonConvert.SerializeObject(task);
                //var buffer = System.Text.Encoding.UTF8.GetBytes(objTask);
                //var byteContent = new ByteArrayContent(buffer);
                var result = client.PostAsJsonAsync(URL, task).Result;

                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
            return View();
        }

        // GET: Task/Edit/5
        public bool Edit(int id, bool status)
        {
            try
            {
                //P.S Should get the Url from Web.Config
                string URL = "http://localhost:59824/api/tasks/" + id;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                var jsonString = "{'status':" + status.ToString().ToLower() + "}";
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                //var buffer = System.Text.Encoding.UTF8.GetBytes(jsonString);
                //var byteContent = new ByteArrayContent(buffer);
                Models.Task task = new Task();
                task.TaskId = id;
                task.IsTaskCompleted = status;
                var result = client.PutAsJsonAsync(URL, task).Result;
            }
            catch (Exception err)
            {
                return false;
            }
            return true;
        }

        // POST: Task/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Task/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Task/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
