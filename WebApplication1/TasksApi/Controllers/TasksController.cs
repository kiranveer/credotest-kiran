﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TasksApi.Models;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

using System.Runtime.Caching;

namespace TasksApi.Controllers
{
    //P.S : Skipping Authentication/Secutity- for time limitation'

    public class TasksController : ApiController
    {
        // GET api/values
        public IEnumerable<Models.Task> Get()
        {
            List<Models.Task> lstTasks = new List<Models.Task>();
            try
            {
                ObjectCache cache = MemoryCache.Default;
                if (cache.Contains("tasks"))
                {
                    lstTasks = (List<Models.Task>)cache.Get("tasks");
                }
            }
            catch //(Exception err)
            {
                //P.S: Should pass Status Code here, And the Method should have the return type as HttpResponseMessage
                // Skipping this for lack of time.

                //var message = string.Format("No Tasks are found");
                // return Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
            }
            return lstTasks;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]Models.Task task)
        {
            List<Models.Task> lstTasks = new List<Task>();
            // Adding insert logic here - Just storing the data in Cache- No DB
            try
            {
                ObjectCache cache = MemoryCache.Default;
                if (cache.Contains("tasks"))
                {
                    lstTasks = (List<Models.Task>)cache.Get("tasks");
                }
                else
                {
                    lstTasks = new List<Task>();
                }
                task.TaskId = lstTasks.Count + 1;
                lstTasks.Add(task);
                cache.Add("tasks", lstTasks, new CacheItemPolicy());

            }
            catch //(Exception err)
            {
                //P.S: Should pass Status Code + Custom Message here, And the Method should have the return type as HttpResponseMessage
                // Skipping this for lack of time.
            }
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]Models.Task task)
        {
            try
            {
                ObjectCache cache = MemoryCache.Default;
                if (cache.Contains("tasks"))
                {
                    List<Models.Task> lstTasks = (List<Models.Task>)cache.Get("tasks");
                    int i = lstTasks.FindIndex(obj => obj.TaskId == id);
                    lstTasks[i].IsTaskCompleted = task.IsTaskCompleted;
                    cache.Add("tasks", lstTasks, new CacheItemPolicy());
                }
            }
            catch //(Exception)
            {

                //throw HttpStatusCode.ExpectationFailed;
            }

        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
