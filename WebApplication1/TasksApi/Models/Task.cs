﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TasksApi.Models
{
    public class Task
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public string TaskDescription { get; set; }
        public bool IsTaskCompleted { get; set; }
    }
}