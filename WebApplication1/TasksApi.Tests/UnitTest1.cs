﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TasksApi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksApi.Controllers.Tests
{
    [TestClass()]
    public class TasksControllerTests
    {
        [TestMethod()]
        public void GetTest()
        {
            TasksController tc = new TasksController();
            List<TasksApi.Models.Task> objTask = (List<TasksApi.Models.Task>)tc.Get();
            Assert.IsNotNull(objTask);
        }

        [TestMethod()]
        public void PostTest()
        {
            TasksController tc = new TasksController();
            TasksApi.Models.Task objTask = new Models.Task();
            objTask.TaskId = 777;
            objTask.TaskName = "TestTask";
            objTask.TaskDescription = "Test Task";
            objTask.IsTaskCompleted = true;
            tc.Post(objTask);
            List<TasksApi.Models.Task> lstTask = (List<TasksApi.Models.Task>)tc.Get();
            Assert.IsNotNull(lstTask);
            Assert.AreEqual(1, lstTask.Count);
        }

        [TestMethod()]
        public void PutTest()
        {
            TasksController tc = new TasksController();
            TasksApi.Models.Task objTask = new Models.Task();
            objTask.TaskId = 888;
            objTask.TaskName = "TestTask";
            objTask.TaskDescription = "Test Task";
            objTask.IsTaskCompleted = true;
            tc.Post(objTask);
            List<TasksApi.Models.Task> lstTask = (List<TasksApi.Models.Task>)tc.Get();

            TasksApi.Models.Task objTask2 = new Models.Task();
            objTask2.TaskId = 888;
            objTask2.TaskName = "TestTask";
            objTask2.TaskDescription = "Updated";
            objTask2.IsTaskCompleted = false;
            tc.Put(777, objTask2);
            List<TasksApi.Models.Task> lstTask2 = (List<TasksApi.Models.Task>)tc.Get();
            Assert.IsNotNull(lstTask);
            Assert.AreEqual(1, lstTask.Count);
            Assert.AreEqual("Updated", lstTask2[0].TaskDescription);
        }
    }
}